package com.msku.mskumobil;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
 
public class fakulte extends Activity {
 public void onCreate(Bundle savedInstanceState) {
 super.onCreate(savedInstanceState);

 setContentView(R.layout.fakulte);
 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
 
 }
 
 
 public void goToFak1 (View view) {
	 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
	 intent.setData(Uri.parse("http://www.edebiyat.mu.edu.tr/"));
	 startActivity(intent);
 }
 
 public void goToFak2 (View view) {
	 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
	 intent.setData(Uri.parse("http://www.egitim.mu.edu.tr/"));
	 startActivity(intent);
 }
 
 public void goToFak3 (View view) {
	 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
	 intent.setData(Uri.parse("http://www.fen.mu.edu.tr/"));
	 startActivity(intent);
 }
 
 public void goToFak4 (View view) {
	 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
	 intent.setData(Uri.parse("http://www.fethiye.mu.edu.tr/"));
	 startActivity(intent);
 }
 
 public void goToFak5 (View view) {
	 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
	 intent.setData(Uri.parse("http://www.gsf.mu.edu.tr/"));
	 startActivity(intent);
 }
 
 public void goToFak6 (View view) {
	 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
	 intent.setData(Uri.parse("http://www.iibf.mu.edu.tr/"));
	 startActivity(intent);
 }
 
 public void goToFak7 (View view) {
	 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
	 intent.setData(Uri.parse("http://www.ilahiyat.mu.edu.tr/"));
	 startActivity(intent);
 }
 
 public void goToFak8 (View view) {
	 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
	 intent.setData(Uri.parse("http://www.mimarlik.mu.edu.tr/"));
	 startActivity(intent);
 }
 
 public void goToFak9 (View view) {
	 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
	 intent.setData(Uri.parse("http://www.muhendislik.mu.edu.tr/"));
	 startActivity(intent);
 }
 
 public void goToFak10 (View view) {
	 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
	 intent.setData(Uri.parse("http://www.sufak.mu.edu.tr/"));
	 startActivity(intent);
 }
 
 public void goToFak11 (View view) {
	 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
	 intent.setData(Uri.parse("http://www.tef.mu.edu.tr/"));
	 startActivity(intent);
 }
 
 public void goToFak12 (View view) {
	 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
	 intent.setData(Uri.parse("http://www.teknoloji.mu.edu.tr/"));
	 startActivity(intent);
 }
 
 public void goToFak13 (View view) {
	 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
	 intent.setData(Uri.parse("http://www.tip.mu.edu.tr/"));
	 startActivity(intent);
 }
 
}