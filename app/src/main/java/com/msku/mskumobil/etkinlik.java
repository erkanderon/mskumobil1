package com.msku.mskumobil;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;


public class etkinlik extends Activity {

	private ProgressDialog pDialog;
	public TextView tv;
	
	JSONArray jArray = null;
	JSONParser jParser = new JSONParser();
	
	String result = "";
	String FILENAME = "etkinlik_cache";
	InputStream iss = null;

	public static final String url = "http://mu.edu.tr/Json.aspx?json=Bugun";

	public String[] etkinlikTipleri = {"Tümü", "Sergi", "Panel", "Tiyatro", "Konferans", "Seminer", "Dinleti", "Konser", "Şenlik", "Bale", "Etkinlik", "Söyleşi", "Yürüyüş", "Kongre", "Yarışma", "Çalıştay", "Kültür Etkinliği", "Sempozyum", "Toplantı", "Kolokyum"};

	public String gelecekresult = "";

	ArrayList<Etkinlik_Object> ets = new ArrayList<Etkinlik_Object>();

	MSKUAdapter adapter;

	List<String> ets1 = new ArrayList<String>();

	Etkinlik_Object etkin;

	JSONObject json_data;


	public void startGelecek(View view) {
		Intent intent = new Intent(this, Geleceketkinlikler.class);
		startActivity(intent);
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.etkinlik);

		tv = (TextView)findViewById(R.id.TextView01);
		
		if (JSONParser.isOnline(getApplicationContext()) != true) {
			jArray = null;
			try {
				iss = openFileInput(FILENAME);
				result = JSONParser.IStoString(iss);

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (result == ""){
				result = "Bugün için etkinlik bulunmamaktadır.";
			}

			tv.setText(result);
		}
		else{
		new EtkinlikCek().execute();}
	}
	private class EtkinlikCek extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(etkinlik.this);
			pDialog.setMessage(getResources().getString(R.string.yukleniyor));
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {
			
			
			jArray = jParser.getJSONFromUrl(url);

			


			

			
				try {

					// looping through All Contacts
					for(int i = 0; i < jArray.length(); i++){
						json_data = jArray.getJSONObject(i);
						etkin = new Etkinlik_Object(json_data);
						// put your parsing code here
						ets.add(etkin);



					}
				} catch (JSONException e) {
					e.printStackTrace();
				}


				for(int i = 0; i < jArray.length(); i++) {

					ets1.add(ets.get(i).getEt());
					result = result + ets.get(i).getEt();

				}

				FileOutputStream fos=null;
				try {
					fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
					fos.write(result.getBytes());
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			
			
			
			
			return null;
		}
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// Dismiss the progress dialog
			if (pDialog.isShowing())
				pDialog.dismiss();
			
			if (!ets1.isEmpty()) {

				adapter = new MSKUAdapter(etkinlik.this, R.layout.lview2, ets);
				ListView listview = (ListView)findViewById(R.id.listview01);
				listview.setAdapter(adapter);


				listview.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub

						Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
						intent.setData(Uri.parse("http://www.mu.edu.tr" + ets.get(arg2).getEtlink()));
						startActivity(intent);
					}
				});

			}

			else {

				tv.setText("Bugün için etkinlik bulunmamaktadır.");
			}
			}
	}
}