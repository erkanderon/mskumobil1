package com.msku.mskumobil;
 
import android.app.TabActivity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TabHost;
 
@SuppressWarnings("deprecation")
public class MSKUActivity extends TabActivity {
 /** Called when the activity is first created. */

@Override
 public void onCreate(Bundle savedInstanceState) {
 super.onCreate(savedInstanceState);
 
 setContentView(R.layout.activity_msku);
 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

 
 Resources res = getResources();
 TabHost tb = getTabHost();
 TabHost.TabSpec spec;
 Intent intent;
 
 //home tab
 intent = new Intent().setClass(this, home.class);
 spec = tb.newTabSpec("tab1").setIndicator("",res.getDrawable(R.drawable.home))
 .setContent(intent);
 tb.addTab(spec);

 //etkinlik tab
 intent = new Intent().setClass(this, etkinlik.class);
 spec = tb.newTabSpec("tab2").setIndicator("",res.getDrawable(R.drawable.etkinlikler))
 .setContent(intent);
 tb.addTab(spec);
 
 //duyuru tab
 intent = new Intent().setClass(this, duyuru.class);
 spec = tb.newTabSpec("tab3").setIndicator("",res.getDrawable(R.drawable.duyurular))
 .setContent(intent);
 tb.addTab(spec);

 // yemekhane tab
 intent = new Intent().setClass(this, yemekhane.class);
 spec = tb.newTabSpec("tab4").setIndicator("",res.getDrawable(R.drawable.yemekhane))
 .setContent(intent);
 tb.addTab(spec);
 
 //rehber tab
 intent = new Intent().setClass(this, Rehber.class);
 spec = tb.newTabSpec("tab5").setIndicator("",res.getDrawable(R.drawable.rehber))
 .setContent(intent);
 tb.addTab(spec);
 
 
 tb.setCurrentTab(0);
 
 }

 public boolean onCreateOptionsMenu(Menu menu) {
	 MenuInflater inflater = getMenuInflater();
	 inflater.inflate(R.menu.activity_msku, menu);
	 return true;
 }
 public boolean onOptionsItemSelected(MenuItem item){
	 switch (item.getItemId()) {
	    case R.id.about:
	    	 startActivity(new Intent(this, about.class));
	    return true;
	    case R.id.harita:
	    	 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
			 intent.setData(Uri.parse("http://harita.mu.edu.tr"));
			 startActivity(intent);
		return true;
	}
	return true;
 }
 
}