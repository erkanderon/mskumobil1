package com.msku.mskumobil;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class yemekhane extends Activity {
	private ProgressDialog pDialog;
	String day;
	String date;
	String Food1;
	String Food1Cal;
	String Food2;
	String Food2Cal;
	String Food3;
	String Food3Cal;
	String Food4;
	String Food4Cal;
	String Sum;
	public TextView by;
	public String sonuc;
	/*public String sonuc;
	JSONArray jArray = null;
	public TextView by;
	JSONParser jParser = new JSONParser();
	String FILENAME = "yemek_cache";
	InputStream iss = null;
	public static final String url = "http://mu.edu.tr/Json.aspx?json=gunlukyemek";
	
	public String toplamKalori = "";*/
	public static final String url = "http://www.mu.edu.tr/yemek.asmx/GetYemek?yemekturu=gunlukyemek";
	private final static String TAG = yemekhane.class.getSimpleName();
	ArrayList<String> items = new ArrayList<String>();
	ArrayAdapter<String> listadapter;
	
	public void startAylik(View view) {
	    Intent intent = new Intent(this, Aylikyemek.class);
	    startActivity(intent);
	}
	
	public void onCreate(Bundle savedInstanceState) {
  
		super.onCreate(savedInstanceState);
		setContentView(R.layout.yemekhane);

		new YemekhaneCek().execute();





		by = (TextView) findViewById(R.id.yemek1);
		/*if (JSONParser.isOnline(getApplicationContext()) != true) {
			jArray = null;
			try {
				iss = openFileInput(FILENAME);
				sonuc = JSONParser.IStoString(iss);
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			by.setText(sonuc);
		}
		else{
		new YemekhaneCek().execute();
	}
		*/}
	public InputStream getUrlData(String url) throws URISyntaxException, ClientProtocolException, IOException {

		DefaultHttpClient client = new DefaultHttpClient();
		HttpGet method = new HttpGet(new URI(url));
		HttpResponse res = client.execute(method);
		return res.getEntity().getContent();
	}
	private class YemekhaneCek extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(yemekhane.this);
			pDialog.setMessage(getResources().getString(R.string.yukleniyor));
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {

			try{
				XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
				factory.setNamespaceAware(true);
				XmlPullParser xpp = factory.newPullParser();
				xpp.setInput(new InputStreamReader(
						getUrlData(url)));
				while (xpp.getEventType() != XmlPullParser.END_DOCUMENT){
					Log.i(TAG, "doc started");
					if (xpp.getEventType() == XmlPullParser.START_TAG) {
						System.out.println(xpp.getName());

						if (xpp.getName().equals("Gun")) {
							day = xpp.nextText();
						}else if(xpp.getName().equals("Tarih")){
							date = xpp.nextText();
						}else if(xpp.getName().equals("Yemek1")){
							Food1 = xpp.nextText();
						}else if(xpp.getName().equals("Yemek1Kalori")){
							Food1Cal = xpp.nextText();
						}else if(xpp.getName().equals("Yemek2")){
							Food2 = xpp.nextText();
						}else if(xpp.getName().equals("Yemek2Kalori")){
							Food2Cal = xpp.nextText();
						}else if(xpp.getName().equals("Yemek3")){
							Food3 = xpp.nextText();
						}else if(xpp.getName().equals("Yemek3Kalori")){
							Food3Cal = xpp.nextText();
						}else if(xpp.getName().equals("Yemek4")){
							Food4 = xpp.nextText();
						}else if(xpp.getName().equals("Yemek4Kalori")){
							Food4Cal = xpp.nextText();
						}else if(xpp.getName().equals("toplam")){
							Sum = xpp.nextText();
						}else {

						}
					}
					xpp.next();
				}

			}catch (Throwable t) {
				Log.d("error", t.toString());
			}
			sonuc = "     " + Food1 + "\n     " + Food2 + "\n     " + Food3 + "\n     " + Food4;
		
		return null;
		}
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// Dismiss the progress dialog
			if (pDialog.isShowing())
				pDialog.dismiss();


			by.setText(sonuc);



		


		}
		
	}
	}