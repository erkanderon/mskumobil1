package com.msku.mskumobil;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.PowerManager;

public class AlarmReciever extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();
        
        String hour = intent.getStringExtra("HOUR");
        String strminute = intent.getStringExtra("MINUTE");
        String name = intent.getStringExtra("NAME");
        // Put here YOUR code.
        Notification.Builder mBuilder = new Notification.Builder(context)
        .setContentTitle(name)
        .setContentText("Etkinlik başlama saati " + hour + ":" + strminute)
        //.setContentText("Saat " + hour + ":" + strminute + " 'da \n" + name + "\n adlı etkinlik yapılacaktır.")
        .setSmallIcon(R.drawable.ic_launcher);
        NotificationManager mNotificationManager =
        	    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);



    mNotificationManager.notify(0, mBuilder.build());

        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(context, notification);
        r.play();
        wl.release();
		
	}
	
	

}
