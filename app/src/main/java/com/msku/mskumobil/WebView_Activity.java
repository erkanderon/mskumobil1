package com.msku.mskumobil;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;

@SuppressLint("SetJavaScriptEnabled")
public class WebView_Activity extends Activity {
	
	WebView myWebView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview_activity);
					
			myWebView = (WebView) findViewById(R.id.webView);
			myWebView.setWebViewClient(new ourViewClient());
			myWebView.getSettings().setJavaScriptEnabled(true);
			myWebView.getSettings().setLoadWithOverviewMode(true);
			myWebView.getSettings().setBuiltInZoomControls(true);
			myWebView.getSettings().setUseWideViewPort(true);
			myWebView.loadUrl(this.getIntent().getDataString());
	}
	/*
	@Override
	public void onBackPressed() {
	    if(myWebView.canGoBack())
	    	myWebView.goBack();
	    else
	        super.onBackPressed();
	}*/
	
	public void goBack(View view) {
		myWebView.goBack();
    }
	
	public void Refresh(View view) {
		myWebView.reload();
    }
	
	public void goForward(View view) {
		myWebView.goForward();
    }
	
	public boolean onCreateOptionsMenu(Menu menu) {
	    super.onCreateOptionsMenu(menu); // Add menu items, second value is the id, use this in the onCreateOptionsMenu
	    menu.add(0, 1, 0, "Back");
	    menu.add(0, 2, 0, "Refresh");
	    menu.add(0, 3, 0, "Forward");
	    return true; // End of menu configuration
	}
	
	public boolean onOptionsItemSelected(MenuItem item) { // Called when you tap a menu item
	    switch (item.getItemId()){
	        case 1: //If the ID equals 1, go back
	        	myWebView.goBack();
	        return true;
	        case 2 : //If the ID equals 2, refresh
	        	myWebView.reload();
	        return true;
	        case 3: //If the ID equals 3, go forward
	        	myWebView.goForward();
	        return true;
	        }
	    return false;
	}
	
	/*
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) { // Enables browsing to previous pages with the hardware back button
	    if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) { // Check if the key event was the BACK key and if there's history
	    	myWebView.goBack();
	        return true;
	    }   // If it wasn't the BACK key or there's no web page history, bubble up to the default
	        // system behavior (probably exit the activity)
	    return super.onKeyDown(keyCode, event);
	} */

}
