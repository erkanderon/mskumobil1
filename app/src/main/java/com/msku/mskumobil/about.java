package com.msku.mskumobil;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class about extends Activity {
	
	private void goToUrl (String url) {
	     Uri uriUrl = Uri.parse(url);
	     Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
	     startActivity(launchBrowser);
	 }
	
	public void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);

		 setContentView(R.layout.about);
		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	public void goToFace (View view) {
	     Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("https://www.facebook.com/mskumobil"));
		 startActivity(intent);
	}
	 
	public void goToTwitter (View view) {
	     goToUrl ( "https://twitter.com/mskuandroid");
	}
}
