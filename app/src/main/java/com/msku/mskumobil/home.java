package com.msku.mskumobil;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
 
public class home extends Activity {
	
	 public void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
		 setContentView(R.layout.home);
	 

	 }
	 
	 public void startHarita(View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://harita.mu.edu.tr"));
		 startActivity(intent);
	 }
	 
	 public void startOBS(View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://obs.mu.edu.tr"));
		 startActivity(intent);
	 }
	 
	 
	 public void startMail(View view) {
		    Intent intent = new Intent(this, Mail.class);
		    startActivity(intent);
	 }
	 
	 public void startAkademik(View view) {
		    Intent intent = new Intent(this, Akademik.class);
		    startActivity(intent);
	 }
	 
	 public void startAbout(View view) {
		    Intent intent = new Intent(this, about.class);
		    startActivity(intent);
	 }
	 
}