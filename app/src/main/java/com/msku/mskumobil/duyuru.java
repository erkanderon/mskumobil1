package com.msku.mskumobil;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class duyuru extends ListActivity {

	public static final String url = "http://mu.edu.tr/Json.aspx?json=Duyuru";
	private ProgressDialog pDialog;
	public List<String> duyurular;

	String FILENAME = "duyuru_cache";
	JSONParser jParser = new JSONParser();
	InputStream iss = null;
	JSONArray jArray = null;	
	public TextView tv;
	public List<String> duyurulinkleri;
	public ListAdapter adapter;
	String result = "";
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.duyuru);
		ListView lv = getListView();

		tv = (TextView)findViewById(R.id.TextView01);
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
				intent.setData(Uri.parse("http://www.mu.edu.tr" + duyurulinkleri.get(arg2)));
				startActivity(intent);
			}
		});

		if (JSONParser.isOnline(getApplicationContext()) != true) {
			jArray = null;
			try {
				iss = openFileInput(FILENAME);
				result = JSONParser.IStoString(iss);

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			tv.setText(result);
		}
		else{
			new DuyuruCek().execute();
		}
	}
	private class DuyuruCek extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(duyuru.this);
			pDialog.setMessage(getResources().getString(R.string.yukleniyor));
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {

			List<String> duyurubasliklari = new ArrayList<String>();
			List<String> duyurubolumleri = new ArrayList<String>();
			List<String> duyurubastarihleri = new ArrayList<String>();
			duyurulinkleri = new ArrayList<String>();
			duyurular = new ArrayList<String>();




			jArray = jParser.getJSONFromUrl(url);

			try {

				// looping through All Contacts
				for(int i = 0; i < jArray.length(); i++){
					JSONObject json_data = jArray.getJSONObject(i);

					// put your parsing code here
					duyurubasliklari.add(json_data.getString("Baslik"));
					duyurubolumleri.add(json_data.getString("Bolum"));
					duyurubastarihleri.add(json_data.getString("OlayBasTarih"));
					duyurulinkleri.add(json_data.getString("Link"));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			for (int i=0;i<jArray.length();i++) {

				duyurular.add("\n" + duyurubasliklari.get(i) + "\n" + duyurubolumleri.get(i) + "\n" + duyurubastarihleri.get(i) + "\n");
				result = result + duyurubasliklari.get(i) + "\n" + duyurubolumleri.get(i) + "\n" + duyurubastarihleri.get(i) + "\n\n";

			}

			FileOutputStream fos=null;
			try {
				fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
				fos.write(result.getBytes());
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// Dismiss the progress dialog
			if (pDialog.isShowing())
				pDialog.dismiss();	

			adapter = new ArrayAdapter<String>(duyuru.this, android.R.layout.simple_list_item_1, duyurular);

			setListAdapter(adapter);



		}

	}
}