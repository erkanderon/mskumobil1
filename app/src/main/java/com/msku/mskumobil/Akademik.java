package com.msku.mskumobil;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.View;

public class Akademik extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 
		 setContentView(R.layout.akademik);
		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
	}
	 
	 public void goToMSKU (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://mu.edu.tr"));
		 startActivity(intent);
	 }
	 
	 public void startFakulteler(View view) {
		    Intent intent = new Intent(this, fakulte.class);
		    startActivity(intent);
	 }
	 
	 public void startYuksok(View view) {
		    Intent intent = new Intent(this, YuksOk.class);
		    startActivity(intent);
	 }
	 
	 public void startMYO(View view) {
		    Intent intent = new Intent(this, MYO.class);
		    startActivity(intent);
	 }
	 
	 public void startEnstituler(View view) {
		    Intent intent = new Intent(this, Enstituler.class);
		    startActivity(intent);
	 }
	 

}
