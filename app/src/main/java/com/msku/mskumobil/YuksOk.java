package com.msku.mskumobil;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.View;

public class YuksOk extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 
		 setContentView(R.layout.yuksok);
		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
	 
	 public void goToBesyo (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.besyo.mu.edu.tr//"));
		 startActivity(intent);
	 }
	 
	 public void goToSivHav (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.sivilhavacilik.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToFetSag (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.fsyo.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToMugSag (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.msyo.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToYabDil (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.ydyo.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToTurizm (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.turizmyo.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 


}
