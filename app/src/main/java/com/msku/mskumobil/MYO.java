package com.msku.mskumobil;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.View;

public class MYO extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 
		 setContentView(R.layout.myo);
		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
	 
	 
	 public void goToBodDen (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.bdmyo.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToDalaman (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.dalamanmyo.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToDatca (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.datcamyo.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToFethiye (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.fethiyemyo.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToIcmeler (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.icmelermyo.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToKoycegiz (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.koycegizmyo.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToMilas (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.milasmyo.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToMugla (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.muglamyo.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToOrtaca (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.ortacamyo.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToSaglik (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.saglikmyo.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToUla (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.ulamyo.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToYatagan (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.yataganmyo.mu.edu.tr/"));
		 startActivity(intent);
	 }



}
