package com.msku.mskumobil;


import org.json.JSONException;
import org.json.JSONObject;


public class Etkinlik_Object {
	
	
	private String etbaslik;
	private String etbastarih;
	private String etlink;
	private String ettip;
	private String et;
	
	public void getData(JSONObject jobj) {
		try {
			etbaslik = jobj.getString("Baslik");
			etbastarih = jobj.getString("OlayBasTarih");
			etlink = jobj.getString("Link");
			ettip = jobj.getString("Tip");
			et = "\n" + etbaslik + "\n" + etbastarih + "\n" + ettip + "\n";
			
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
	}
	
	public Etkinlik_Object(JSONObject jobj) {
		
		getData(jobj);
	}
	public int getYear() {
		return Integer.parseInt(etbastarih.substring(6, 10));
	}
	
	public int getMonth() {
		return Integer.parseInt(etbastarih.substring(3, 5))-1;
	}

	public int getDay() {
		return Integer.parseInt(etbastarih.substring(0, 2));
	}
	
	public String getHour() {
		return etbastarih.substring(11, 13);
	}
	
	public String getMinute() {
		return etbastarih.substring(14, 16);
	}
	
	public String getEtbaslik() {
		return etbaslik;
	}

	public String getEtbastarih() {
		return etbastarih;
	}

	public String getEtlink() {
		return etlink;
	}

	public String getEttip() {
		return ettip;
	}

	public String getEt() {
		return et;
	}
	

	
}
