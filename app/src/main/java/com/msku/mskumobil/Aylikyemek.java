package com.msku.mskumobil;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;


public class Aylikyemek extends Activity {
	private ProgressDialog pDialog;
	public TextView ay;
	public String sonuc = "";
	JSONArray jArray = null;
	JSONParser jParser = new JSONParser();
	String FILENAME = "aylik_yemek_cache";
	InputStream iss = null;
	
	public static String giveDate() {
		
		final Calendar c = Calendar.getInstance();
    	int yy = c.get(Calendar.YEAR);
    	int mm = c.get(Calendar.MONTH);
    	int dd = c.get(Calendar.DAY_OF_MONTH);
    	
    	StringBuilder sb = new StringBuilder().append(dd).append(".").append(mm + 1).append(".").append(yy);
    	
    	String date = sb.toString();
    	return date;
    	
    }
	
	public static String give1monthlater() {
		
		final Calendar c = Calendar.getInstance();
    	int yy = c.get(Calendar.YEAR);
    	int mm = c.get(Calendar.MONTH);
    	int dd = c.get(Calendar.DAY_OF_MONTH);
    	
    	StringBuilder sb = null;
        if (mm < 11){
        	sb = new StringBuilder().append(dd).append(".").append(mm + 2).append(".").append(yy);
    	}
    	else if (mm == 11) {
    		sb = new StringBuilder().append(dd).append(".").append(1).append(".").append(yy+1);
    	}
    	
    	String date = sb.toString();
    	return date;
    	
    }
	
	
	public static final String url = "http://mu.edu.tr/Json.aspx?json=yemek&bastarih=" + giveDate() + "&bittarih=" + give1monthlater();

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 
		 setContentView(R.layout.aylikyemek);
		 ay = (TextView) findViewById(R.id.aylikText);	
		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		 if (JSONParser.isOnline(getApplicationContext()) != true) {
				jArray = null;
				try {
					iss = openFileInput(FILENAME);
					sonuc = JSONParser.IStoString(iss);
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ay.setText(sonuc);
		 }
		 else{
			 new AylikYemekCek().execute(); 
		 }
	}
	
	
	private class AylikYemekCek extends AsyncTask<Void, Void, Void> {
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(Aylikyemek.this);
			pDialog.setMessage(getResources().getString(R.string.yukleniyor));
			pDialog.setCancelable(false);
			pDialog.show();

		}
		@Override
		protected Void doInBackground(Void... params) {
			
			 jArray = jParser.getJSONFromUrl(url);
			 
			 List<String> yemekgunleri = new ArrayList<String>();
			 List<String> yemektarihleri = new ArrayList<String>();
			 List<String> yemek1 = new ArrayList<String>();
			 List<String> yemek2 = new ArrayList<String>();
			 List<String> yemek3 = new ArrayList<String>();
			 List<String> yemek4 = new ArrayList<String>();
			 		 
				 try {
					  // looping through All Contacts
					  for(int i = 0; i < jArray.length(); i++){
						 
						  JSONObject json_data = jArray.getJSONObject(i);

					      // put your parsing code here
						  yemekgunleri.add(json_data.getString("Gun"));
						  yemektarihleri.add(json_data.getString("Tarih"));
					      yemek1.add(json_data.getString("Yemek1"));
					      yemek2.add(json_data.getString("Yemek2"));
					      yemek3.add(json_data.getString("Yemek3"));
					      yemek4.add(json_data.getString("Yemek4"));
						  
						}
				 } catch (JSONException e) {
							    e.printStackTrace();
			     }
				 
				 for (int i=0;i<jArray.length();i++) {
						
					 sonuc = sonuc + yemektarihleri.get(i) + "  " + yemekgunleri.get(i) + "\n         " + yemek1.get(i) + "\n         " + yemek2.get(i) + "\n         " + yemek3.get(i) + "\n         " + yemek4.get(i) + "\n\n";
						
				 }
				 
				 FileOutputStream fos=null;
				 try {
			     		fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
						fos.write(sonuc.getBytes());
						fos.close();
				 } catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
				 }
				 
			 
			  
			 
			return null;
		}
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// Dismiss the progress dialog
			if (pDialog.isShowing())
				pDialog.dismiss();
			
			
			 ay.setText(sonuc);
			}	
	}
	
}
		/**/
		 


