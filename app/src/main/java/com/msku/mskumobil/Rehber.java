package com.msku.mskumobil;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class Rehber extends Activity implements OnItemClickListener {
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rehber);
		
		ListView listview = (ListView) findViewById(R.id.rehberList);
        listview.setOnItemClickListener(this);
    }
	
	
	String[] nameList = {"Alo Güvenlik\n(Acil Durum ve İhbar Hattı)",   "Santral",   "Sağlık Merkezi",  "Kütüphane",  "Öğrenci İşleri Dairesi Başkanlığı", "Uluslararası İlişkiler Koordinatörlüğü", "KYK Yurdu\n(Danışma)",  "SKS Daire Başkanlığı", "Sıtkı Koçman Vakfı"};
	String[] phoneList = {            "02522111111",                  "02522111000",  "02522111063",   "02522111071",            "02522111251",                             "02522111960",               "02522238011",          "02522111161",         "02522111979"};
	
    @SuppressWarnings("deprecation")
	public void onItemClick(AdapterView<?> l, View v, int position, long id) {
        final String ph = phoneList[position];
        String nm = nameList[position];
    	AlertDialog callDialog = new AlertDialog.Builder(this).create();                 
        callDialog.setTitle(nm);
        callDialog.setMessage(ph);
        callDialog.setIcon(R.drawable.callicon);
        
        callDialog.setButton("Arama Yap", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
				Intent callIntent = new Intent(Intent.ACTION_CALL);
				callIntent.setData(Uri.parse("tel: +90%s" + ph));
				startActivity(callIntent);
            }
            
        });
        
        callDialog.show();
    }

}
