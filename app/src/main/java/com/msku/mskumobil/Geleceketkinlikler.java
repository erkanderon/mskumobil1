package com.msku.mskumobil;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class Geleceketkinlikler extends Activity {

	private ProgressDialog pDialog;
	public TextView tv;
	
	JSONArray jArray = null;
	JSONParser jParser = new JSONParser();
	String result = "";
	String FILENAME = "gelecek_etkinlik_cache";
	InputStream iss = null;
	
	public static final String url = "http://mu.edu.tr/Json.aspx?json=Etkinlik";
	public String[] etkinlikTipleri = {"Tümü", "Sergi", "Panel", "Tiyatro", "Konferans", "Seminer", "Dinleti", "Konser", "Şenlik", "Bale", "Etkinlik", "Söyleşi", "Yürüyüş", "Kongre", "Yarışma", "Çalıştay", "Kültür Etkinliği", "Sempozyum", "Toplantı", "Kolokyum"};
	ArrayList<Etkinlik_Object> ets = new ArrayList<Etkinlik_Object>();
	MSKUAdapter adapter;
	List<String> ets1 = new ArrayList<String>();
	Etkinlik_Object etkin;
	JSONObject json_data;
	String setkinlik , Strminute , Strhour;
	static final int TIME_DIALOG_ID=1;
	private int year , month , day , hour , minute;
	public int shour , sminute = 0;
	Calendar calendar = Calendar.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.geleceketkinlikler);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		tv = (TextView)findViewById(R.id.TextView01);
		

		if (JSONParser.isOnline(getApplicationContext()) != true) {
			try {
				iss = openFileInput(FILENAME);
				result = JSONParser.IStoString(iss);

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			tv.setText(result);
		}
	
	else{
		new GeleceketkinliklerCek().execute();
	}}
	private class GeleceketkinliklerCek extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
			pDialog = new ProgressDialog(Geleceketkinlikler.this);
			pDialog.setMessage(getResources().getString(R.string.yukleniyor));
			pDialog.setCancelable(false);
			pDialog.show();

		}

		@Override
		protected Void doInBackground(Void... arg0) {
		
		
		jArray = jParser.getJSONFromUrl(url);

			try {

				// looping through All Contacts
				for(int i = 0; i < jArray.length(); i++){
					json_data = jArray.getJSONObject(i);
					etkin = new Etkinlik_Object(json_data);
					// put your parsing code here
					ets.add(etkin);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			for(int i = 0; i < jArray.length(); i++) {

				ets1.add(ets.get(i).getEt());
				result = result + ets.get(i).getEt();
			}

			FileOutputStream fos = null;
			try {
				fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
				fos.write(result.getBytes());
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return null;
			 
			
		

	}
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// Dismiss the progress dialog
			if (pDialog.isShowing())
				pDialog.dismiss();
			
			if (!ets1.isEmpty()) {

				adapter = new MSKUAdapter(Geleceketkinlikler.this, R.layout.lview, ets);
				ListView listview = (ListView)findViewById(R.id.listview02);
				listview.setAdapter(adapter);

				listview.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
						intent.setData(Uri.parse("http://www.mu.edu.tr" + ets.get(arg2).getEtlink()));
						startActivity(intent);
					}
				});

			}

			else {

				tv.setText("Etkinlik bulunmamaktadır.");
			}
			}
	}
	
	
	
	private TimePickerDialog.OnTimeSetListener mTimeSetListener =
			new TimePickerDialog.OnTimeSetListener() {
		// the callback received when the user "sets" the TimePickerDialog in the dialog
		public void onTimeSet(TimePicker view, int hourOfDay, int min) {
			shour = hourOfDay;
			sminute = min;
			// Set the Selected Date in Select date Button

			Intent intentAlarm = new Intent(Geleceketkinlikler.this, AlarmReciever.class);
			intentAlarm.putExtra("HOUR", Strhour);
			intentAlarm.putExtra("MINUTE", Strminute);
			intentAlarm.putExtra("NAME", setkinlik);

			calendar.set(year,month,day,shour,sminute);

			AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

			alarmManager.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(), 
					PendingIntent.getBroadcast(Geleceketkinlikler.this, 1,  
							intentAlarm, 
							PendingIntent.FLAG_UPDATE_CURRENT));
			Toast.makeText(Geleceketkinlikler.this, "Alarm kuruldu", Toast.LENGTH_LONG).show();
		}
	};

	@SuppressWarnings("deprecation")
	public void alarmKur(View v) {
		Etkinlik_Object item = (Etkinlik_Object)v.getTag();
		year = item.getYear();
		month = item.getMonth();
		day = item.getDay();
		Strhour = item.getHour();
		hour = Integer.parseInt(Strhour);
		Strminute = item.getMinute();
		minute = Integer.parseInt(Strminute);
		setkinlik = item.getEtbaslik();
		showDialog(TIME_DIALOG_ID);
	}

	public void linkeGit(View v) {
		Etkinlik_Object item = (Etkinlik_Object)v.getTag();
		Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		intent.setData(Uri.parse("http://mu.edu.tr" + item.getEtlink()));
		startActivity(intent);
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		// create a new TimePickerDialog with values you want to show
		TimePickerDialog tpd = new TimePickerDialog(this, mTimeSetListener, hour, minute, true);
		return tpd;
	}

}
	