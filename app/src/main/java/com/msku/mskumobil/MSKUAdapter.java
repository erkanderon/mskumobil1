package com.msku.mskumobil;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

public class MSKUAdapter extends ArrayAdapter<Etkinlik_Object>{
	
	private Context context;
	private int layoutResourceId;
	private List<Etkinlik_Object> items;

	public MSKUAdapter(Context context, int layoutResourceId, List<Etkinlik_Object> items) {
		super(context, layoutResourceId, items);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.items = items;
		
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		MSKUHolder holder = null;

		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		row = inflater.inflate(layoutResourceId, parent, false);

		holder = new MSKUHolder();
		holder.etkinliks = items.get(position);
		holder.alarmKur = (Button)row.findViewById(R.id.alarmbutton);
		holder.alarmKur.setTag(holder.etkinliks);
		
		holder.linkeGit = (Button)row.findViewById(R.id.linkbutton);
		holder.linkeGit.setTag(holder.etkinliks);

		holder.etText = (TextView)row.findViewById(R.id.TextView02);

		row.setTag(holder);

		setupItem(holder);
		return row;
	}
	
	private void setupItem(MSKUHolder holder) {
		holder.etText.setText(holder.etkinliks.getEt());
	}

	public static class MSKUHolder {
		Etkinlik_Object etkinliks;
		TextView etText;
		Button alarmKur;
		Button linkeGit;
	}

	

}
