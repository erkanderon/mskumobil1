package com.msku.mskumobil;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.View;

public class Enstituler extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		 setContentView(R.layout.enstituler);
		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
	
	 public void goToEgitBil (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.egitimbilimleri.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToFenBil (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse( "http://www.fenbilimleri.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToSagBil (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.saglikbilimleri.mu.edu.tr/"));
		 startActivity(intent);
	 }
	 
	 public void goToSosBil (View view) {
		 Intent intent = new Intent(getBaseContext(), WebView_Activity.class);
		 intent.setData(Uri.parse("http://www.sosyalbilimler.mu.edu.tr/"));
		 startActivity(intent);
	 }


}
