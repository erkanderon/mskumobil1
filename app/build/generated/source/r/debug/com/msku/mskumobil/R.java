/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.msku.mskumobil;

public final class R {
    public static final class array {
        public static final int rehber=0x7f060000;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int MSKUBlue=0x7f070000;
        public static final int White=0x7f070001;
    }
    public static final class dimen {
        public static final int activity_horizontal_margin=0x7f040000;
        public static final int activity_vertical_margin=0x7f040001;
    }
    public static final class drawable {
        public static final int back=0x7f020000;
        public static final int buttonbar=0x7f020001;
        public static final int buttonbar2=0x7f020002;
        public static final int callicon=0x7f020003;
        public static final int clock=0x7f020004;
        public static final int duyurular=0x7f020005;
        public static final int etkinlikler=0x7f020006;
        public static final int facebookicon=0x7f020007;
        public static final int fakulte=0x7f020008;
        public static final int fakultemiz=0x7f020009;
        public static final int forward=0x7f02000a;
        public static final int harita=0x7f02000b;
        public static final int home=0x7f02000c;
        public static final int ic_launcher=0x7f02000d;
        public static final int info=0x7f02000e;
        public static final int link_btn=0x7f02000f;
        public static final int link_btn_pressed=0x7f020010;
        public static final int link_btn_states=0x7f020011;
        public static final int main_btn=0x7f020012;
        public static final int main_btn_pressed=0x7f020013;
        public static final int main_btn_states=0x7f020014;
        public static final int ogrenci=0x7f020015;
        public static final int personel=0x7f020016;
        public static final int refresh=0x7f020017;
        public static final int rehber=0x7f020018;
        public static final int skos=0x7f020019;
        public static final int twitter=0x7f02001a;
        public static final int uni1=0x7f02001b;
        public static final int world=0x7f02001c;
        public static final int yemekhane=0x7f02001d;
    }
    public static final class id {
        public static final int TextView01=0x7f0a0010;
        public static final int TextView02=0x7f0a0029;
        public static final int about=0x7f0a003c;
        public static final int action_settings=0x7f0a003e;
        public static final int akabaslik=0x7f0a0007;
        public static final int alarmbutton=0x7f0a002a;
        public static final int aylikText=0x7f0a000e;
        public static final int aylikbutton=0x7f0a0038;
        public static final int aylikyem=0x7f0a000d;
        public static final int button1=0x7f0a0013;
        public static final int button10=0x7f0a0020;
        public static final int button11=0x7f0a0021;
        public static final int button12=0x7f0a0022;
        public static final int button13=0x7f0a0023;
        public static final int button2=0x7f0a0014;
        public static final int button3=0x7f0a0015;
        public static final int button4=0x7f0a0016;
        public static final int button5=0x7f0a001b;
        public static final int button6=0x7f0a001c;
        public static final int button7=0x7f0a001d;
        public static final int button8=0x7f0a001e;
        public static final int button9=0x7f0a001f;
        public static final int duybaslik=0x7f0a000f;
        public static final int enstbaslik=0x7f0a0011;
        public static final int enstituler=0x7f0a000c;
        public static final int etbaslik=0x7f0a0017;
        public static final int fakbaslik=0x7f0a001a;
        public static final int fakulteler=0x7f0a0009;
        public static final int gelecekbutton=0x7f0a0018;
        public static final int geleceketbaslik=0x7f0a0024;
        public static final int harita=0x7f0a003d;
        public static final int imageButton1=0x7f0a0002;
        public static final int imageButton2=0x7f0a0004;
        public static final int imageButton3=0x7f0a0026;
        public static final int imageButton4=0x7f0a0027;
        public static final int imageButton5=0x7f0a0028;
        public static final int imageView1=0x7f0a0000;
        public static final int imageView11=0x7f0a0036;
        public static final int linearLayout1=0x7f0a0035;
        public static final int linkbutton=0x7f0a002b;
        public static final int listview01=0x7f0a0019;
        public static final int listview02=0x7f0a0025;
        public static final int myo=0x7f0a000b;
        public static final int myobaslik=0x7f0a0030;
        public static final int ogrbaslik=0x7f0a002e;
        public static final int ogrbutton=0x7f0a002f;
        public static final int perbaslik=0x7f0a002c;
        public static final int perbutton=0x7f0a002d;
        public static final int rehberList=0x7f0a0033;
        public static final int rehberbaslik=0x7f0a0032;
        public static final int scrollView1=0x7f0a0001;
        public static final int scrollView2=0x7f0a003b;
        public static final int scrollView3=0x7f0a0012;
        public static final int scrollView4=0x7f0a0031;
        public static final int textView1=0x7f0a0003;
        public static final int textView2=0x7f0a0006;
        public static final int textView3=0x7f0a0005;
        public static final int uniweb=0x7f0a0008;
        public static final int webView=0x7f0a0034;
        public static final int yemek1=0x7f0a0039;
        public static final int yemekbaslik=0x7f0a0037;
        public static final int yuksok=0x7f0a000a;
        public static final int yuksokbaslik=0x7f0a003a;
    }
    public static final class layout {
        public static final int about=0x7f030000;
        public static final int activity_msku=0x7f030001;
        public static final int akademik=0x7f030002;
        public static final int aylikyemek=0x7f030003;
        public static final int duyuru=0x7f030004;
        public static final int enstituler=0x7f030005;
        public static final int etkinlik=0x7f030006;
        public static final int fakulte=0x7f030007;
        public static final int geleceketkinlikler=0x7f030008;
        public static final int home=0x7f030009;
        public static final int lview=0x7f03000a;
        public static final int lview2=0x7f03000b;
        public static final int mail=0x7f03000c;
        public static final int myo=0x7f03000d;
        public static final int rehber=0x7f03000e;
        public static final int webview_activity=0x7f03000f;
        public static final int yemekhane=0x7f030010;
        public static final int yuksok=0x7f030011;
    }
    public static final class menu {
        public static final int activity_msku=0x7f090000;
        public static final int webview_menu=0x7f090001;
    }
    public static final class string {
        public static final int about_content=0x7f080000;
        public static final int action_settings=0x7f080001;
        public static final int app_name=0x7f080002;
        public static final int ay_yem=0x7f080003;
        public static final int besyo=0x7f080004;
        public static final int bilg_sis=0x7f080005;
        public static final int fetsag=0x7f080006;
        public static final int harita=0x7f080007;
        public static final int hello_world=0x7f080008;
        public static final int mail=0x7f080009;
        public static final int mail_adrs=0x7f08000a;
        public static final int menu_about=0x7f08000b;
        public static final int menu_enst=0x7f08000c;
        public static final int menu_fak=0x7f08000d;
        public static final int menu_harita=0x7f08000e;
        public static final int menu_myo=0x7f08000f;
        public static final int menu_uni=0x7f080010;
        public static final int menu_yuksok=0x7f080011;
        public static final int mobil_uyg=0x7f080012;
        public static final int mugsag=0x7f080013;
        public static final int neden=0x7f080014;
        public static final int sivhav=0x7f080015;
        public static final int title_activity_akademik=0x7f080016;
        public static final int title_activity_aylikyemek=0x7f080017;
        public static final int title_activity_enstituler=0x7f080018;
        public static final int title_activity_geleceketkinlikler=0x7f080019;
        public static final int title_activity_mail=0x7f08001a;
        public static final int title_activity_myo=0x7f08001b;
        public static final int title_activity_obs=0x7f08001c;
        public static final int title_activity_ogr_mail=0x7f08001d;
        public static final int title_activity_per_mail=0x7f08001e;
        public static final int title_activity_rehber=0x7f08001f;
        public static final int title_activity_web_view_=0x7f080020;
        public static final int title_activity_yuks_ok=0x7f080021;
        public static final int turizm=0x7f080022;
        public static final int uni_name=0x7f080023;
        public static final int version=0x7f080024;
        public static final int web_adrs=0x7f080025;
        public static final int yabdil=0x7f080026;
        public static final int yukleniyor=0x7f080027;
    }
    public static final class style {
        /**  API 11 theme customizations can go here. 
 API 14 theme customizations can go here. 

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        
         */
        public static final int AppBaseTheme=0x7f050000;
        /**  All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f050001;
        public static final int NoActionBar=0x7f050002;
        public static final int baslik_bt=0x7f050003;
        public static final int link_button=0x7f050004;
        public static final int main_bt=0x7f050005;
        public static final int main_image=0x7f050006;
    }
}
